<?php

/**
 * @file
 * Saferpay menu items callbacks.
 */

/**
 * Handles a complete saferpay sale.
 */
function uc_saferpay_complete($order) {
  if (intval($_SESSION['cart_order']) != $order->order_id || $order->payment_method != 'saferpay') {
    drupal_goto('cart');
  }
  parse_str($_SERVER['QUERY_STRING'], $input);
  $data = rawurldecode($input['DATA']);
  while (drupal_strtolower(drupal_substr($data, 0, 14)) == '<idp msgtype=\\') {
    $data = stripslashes($data);
  }
  $signature = rawurldecode($input['SIGNATURE']);
  $debug = variable_get('uc_saferpay_debug', FALSE) ? 'DATA: ' . var_export($data, TRUE) . PHP_EOL . 'SIGNATURE: ' . var_export($signature, TRUE) : '';
  watchdog('uc_saferpay', 'Receiving Payment Complete Notification. <pre>@debug</pre>', array('@debug' => $debug));

  $tx_amount = 0;
  if (preg_match('/^<idp\s.*amount="([0-9]+)".*>$/i', $data, $matches)) {
    $tx_amount = floatval($matches[1]);
  }
  $tx_currency = $order->currency;
  if (preg_match('/^<idp\s.*currency="([A-Z]{3})".*>$/i', $data, $matches)) {
    $tx_currency = $matches[1];
  }
  $payment_provider_id = 0;
  if (preg_match('/^<idp\s.*providerid="([0-9]+)".*>$/i', $data, $matches)) {
    $payment_provider_id = intval($matches[1]);
  }
  $payment_provider_name = '';
  if (preg_match('/^<idp\s.*providername="([^"]+)".*>$/i', $data, $matches)) {
    $payment_provider_name = $matches[1];
  }
  $url = 'https://www.saferpay.com/hosting/VerifyPayConfirm.asp?DATA=' . drupal_encode_path($data) . '&SIGNATURE=' . drupal_encode_path($signature);
  $result = _uc_saferpay_process_url($url);
  if (drupal_substr($result, 0, 3) == 'OK:') {
    parse_str(drupal_substr($result, 3), $result_output);
    $result = db_select('uc_payment_saferpay_tx', 'upst')
        ->fields('upst')
        ->condition('upst.txn_id', $result_output['ID'])
        ->execute();
    $transaction_count = $result->rowCount();
    if (empty($transaction_count)) {
      /*
       * Record payment log
       *   $ID = saferpay transaction identifier, store in DBMS.
       *   $TOKEN = token of transaction, store in DBMS.
       */
      db_insert('uc_payment_saferpay_tx')
          ->fields(array(
            'order_id' => $order->order_id,
            'txn_id' => $result_output['ID'],
            'mc_gross' => $tx_amount / 100,
            'mc_currency' => $tx_currency,
            'provider_id' => $payment_provider_id,
            'provider_name' => $payment_provider_name,
            'received' => time(),
          ))
          ->execute();

      if (variable_get('uc_saferpay_autocomplete', TRUE)) {
        /*         * *** Optional: Finalize payment by capture of transaction **** */
        $url = 'https://www.saferpay.com/hosting/PayComplete.asp?ACCOUNTID=' . variable_get('uc_saferpay_account_id', '99867-94913159&spPassword=XAjc3Kna') . '&ID=' . $result_output['ID'] . '&TOKEN=' . drupal_encode_path($result_output['TOKEN']);
        $result = _uc_saferpay_process_url($url);
        if (drupal_substr($result, 0, 2) == 'OK') {
          $comment = t('Saferpay transaction ID: @txn_id', array('@txn_id' => $result_output['ID']));
          uc_payment_enter($order->order_id, 'saferpay', $tx_amount / 100, $order->uid, NULL, $comment);
          uc_order_comment_save($order->order_id, 0, t('Payment of @amount @currency submitted through Saferpay.', array('@amount' => uc_currency_format($tx_amount / 100), '@currency' => $tx_currency)), 'order', 'payment_received');
          db_update('uc_payment_saferpay_tx')
              ->fields(array(
                'completed' => 1,
                'complete_result' => $result,
              ))
              ->condition('order_id', $order->order_id)
              ->condition('txn_id', $result_output['ID'])
              ->execute();
        }
        else {
          db_update('uc_payment_saferpay_tx')
              ->fields(array(
                'complete_result' => $result,
              ))
              ->condition('order_id', $order->order_id)
              ->condition('txn_id', $result_output['ID'])
              ->execute();
        }
      }
    }
    $_SESSION['uc_checkout'][$_SESSION['cart_order']]['do_complete'] = TRUE;
    drupal_goto('cart/checkout/complete');
  }
  else {
    drupal_set_message(t('An error has occurred in your Saferpay payment. Please review your cart and try again.'));
  }
  drupal_goto('cart');
}

/**
 * Handles a complete saferpay sale.
 */
function uc_saferpay_ipn($order) {
  $data = $_POST['DATA'];
  while (drupal_strtolower(drupal_substr($data, 0, 14)) == '<idp msgtype=\\') {
    $data = stripslashes($data);
  }
  $signature = $_POST['SIGNATURE'];
  $debug = variable_get('uc_saferpay_debug', FALSE) ? 'DATA: ' . var_export($data, TRUE) . PHP_EOL . 'SIGNATURE: ' . var_export($signature, TRUE) : '';
  watchdog('uc_saferpay', 'Receiving Payment Complete Notification via IPN. <pre>@debug</pre>', array('@debug' => $debug));

  $tx_amount = 0;
  if (preg_match('/^<idp\s.*amount="([0-9]+)".*>$/i', $data, $matches)) {
    $tx_amount = floatval($matches[1]);
  }
  $tx_currency = $order->currency;
  if (preg_match('/^<idp\s.*currency="([A-Z]{3})".*>$/i', $data, $matches)) {
    $tx_currency = $matches[1];
  }
  $payment_provider_id = 0;
  if (preg_match('/^<idp\s.*providerid="([0-9]+)".*>$/i', $data, $matches)) {
    $payment_provider_id = intval($matches[1]);
  }
  $payment_provider_name = '';
  if (preg_match('/^<idp\s.*providername="([^"]+)".*>$/i', $data, $matches)) {
    $payment_provider_name = $matches[1];
  }
  $url = 'https://www.saferpay.com/hosting/VerifyPayConfirm.asp?DATA=' . drupal_encode_path($data) . '&SIGNATURE=' . drupal_encode_path($signature);
  $result = _uc_saferpay_process_url($url);
  if (drupal_substr($result, 0, 3) == 'OK:') {
    parse_str(drupal_substr($result, 3), $result_output);
    $result = db_select('uc_payment_saferpay_tx', 'upst')
        ->fields('upst')
        ->condition('upst.txn_id', $result_output['ID'])
        ->execute();
    $transaction_count = $result->rowCount();
    if (empty($transaction_count)) {
      /*
       * Record payment log
       *   $ID = saferpay transaction identifier, store in DBMS.
       *   $TOKEN = token of transaction, store in DBMS.
       */
      db_insert('uc_payment_saferpay_tx')
          ->fields(array(
            'order_id' => $order->order_id,
            'txn_id' => $result_output['ID'],
            'mc_gross' => $tx_amount / 100,
            'mc_currency' => $tx_currency,
            'provider_id' => $payment_provider_id,
            'provider_name' => $payment_provider_name,
            'received' => time(),
          ))
          ->execute();
      if (variable_get('uc_saferpay_autocomplete', TRUE)) {
        /*         * *** Optional: Finalize payment by capture of transaction **** */
        $url = 'https://www.saferpay.com/hosting/PayComplete.asp?ACCOUNTID=' . variable_get('uc_saferpay_account_id', '99867-94913159&spPassword=XAjc3Kna') . '&ID=' . $result_output['ID'] . '&TOKEN=' . drupal_encode_path($result_output['TOKEN']);
        $result = _uc_saferpay_process_url($url);
        if (drupal_substr($result, 0, 2) == 'OK') {
          $comment = t('Saferpay transaction ID: @txn_id', array('@txn_id' => $result_output['ID']));
          uc_payment_enter($order->order_id, 'saferpay', $tx_amount / 100, $order->uid, NULL, $comment);
          uc_order_comment_save($order->order_id, 0, t('Payment of @amount @currency submitted through Saferpay.', array('@amount' => uc_currency_format($tx_amount / 100), '@currency' => $tx_currency)), 'order', 'payment_received');
          db_update('uc_payment_saferpay_tx')
              ->fields(array(
                'completed' => 1,
                'complete_result' => $result,
              ))
              ->condition('order_id', $order->order_id)
              ->condition('txn_id', $result_output['ID'])
              ->execute();
        }
        else {
          db_update('uc_payment_saferpay_tx')
              ->fields(array(
                'complete_result' => $result,
              ))
              ->condition('order_id', $order->order_id)
              ->condition('txn_id', $result_output['ID'])
              ->execute();
        }
      }
    }
  }
  return TRUE;
}

/**
 * Handles a cancelled saferpay sale.
 */
function uc_saferpay_cancel() {
  unset($_SESSION['cart_order']);
  drupal_set_message(t('Your Saferpay payment was cancelled. Please feel free to continue shopping or contact us for assistance.'));
  drupal_goto('cart');
}
