Saferpay Payment Gateway for Ubercart.

CONFIGURATION
-------------

  Store Home » Administration » Store » Configuration
  (admin/store/settings/payment/edit/methods)

There is "Saferpay" checkbox which has to be enabled. Next step is to configure
Saferpay Payment Gateway credentials.

You will need:

* Account ID
* Order identifier - Which is prefilled, If you want you can change it.

GET INVOLVED
------------

Improvements to this documentation, module or feature requests should be
submitted as issue to project issue queue.

Thanks!
